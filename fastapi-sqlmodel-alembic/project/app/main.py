from fastapi import Depends, FastAPI
from sqlalchemy import select
from sqlmodel import Session

from db import get_session, init_db
from models import Song, SongCreate

app = FastAPI()


@app.on_event("startup")
def start_up():
    init_db()


@app.get("/ping")
async def pong():
    return {"ping": "pong!"}


@app.get("/songs", response_model=list[Song])
def get_songs(session: Session = Depends(get_session)):
    result = session.execute(select(Song))
    songs = result.scalars().all()
    return [
        Song(name=song.name, artist=song.artist, year=song.year, id=song.id)
        for song in songs
    ]


@app.post("/songs", response_model=Song)
def add_song(song: SongCreate, session: Session = Depends(get_session)):
    song = Song(name=song.name, artist=song.artist, year=song.year)
    session.add(song)
    session.commit()
    session.refresh(song)

    return song
